# Criação da organização.
resource "aws_organizations_organization" "this" {

  # (Optional) List of AWS service principal names for which you want to enable integration with your organization.
  # This is typically in the form of a URL, such as service-abbreviation.amazonaws.com.
  # Organization must have feature_set set to ALL.
  # For additional information, see the AWS Organizations User Guide.
  aws_service_access_principals = try(var.organization_info.aws_service_access_principals, [
    "cloudtrail.amazonaws.com"
  ])

  # (Optional) List of Organizations policy types to enable in the Organization Root.
  # Organization must have feature_set set to ALL.
  # For additional information about valid policy types 
  # (e.g. AISERVICES_OPT_OUT_POLICY, BACKUP_POLICY, SERVICE_CONTROL_POLICY, and TAG_POLICY), see the AWS Organizations API Reference.
  enabled_policy_types = try(var.organization_info.enabled_policy_types, ["SERVICE_CONTROL_POLICY"])

  # (Optional) Specify "ALL" (default) or "CONSOLIDATED_BILLING".
  feature_set = try(var.organization_info.feature_set, "ALL")

}
