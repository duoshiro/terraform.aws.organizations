# Criação de unidade organizacional.
resource "aws_organizations_organizational_unit" "this" {

  # Rodar por cada OU enviada.
  for_each = var.organization_info.ous

  # The name for the organizational unit.
  name = each.value.ou_name

  # ID of the parent organizational unit, which may be the root.
  parent_id = each.value.ou_parent_id

  # (Optional) Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level.
  tags = lookup(each.value, "ou_tags", [])

}
